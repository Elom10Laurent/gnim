import { useState } from "react";
import Logo from "/src/assets/LOGO-TOGO-GNIM.png";

export const NavBar = () => {
  const [clicked, setClicked] = useState(true);

  const isClicked = () => {
    setClicked(!clicked);
  };

  const burger_Icon = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="w-8 h-8 burger"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
      />
    </svg>
  );

  const X_mark = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="w-8 h-8"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M6 18 18 6M6 6l12 12"
      />
    </svg>
  );

  const screenWidth = window.innerWidth;

  const searchIcon = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className="w-6 h-6"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z"
      />
    </svg>
  );

  const activeMobileNav = "right-full";

  const navigationNavStyle =
    "md:flex  md:items-center  items-start md:justify-between justify-start";

  const SearchInputForLargeScreen = (
    <div className="border-2  rounded-xl h-10 px-3 flex items-center">
      <input className="h-full outline-none" placeholder="Search" />
    </div>
  );

  const navMenu = [
    { link: "Acceuil", url: "/" },
    { link: "Exposition", url: "/about" },
    { link: "Boutiques", url: "/faq" },
    { link: "Produits", url: "/clients" },
    { link: "Soucription", url: "/clients" },
  ];
  return (
    <div className=" py-2  ">
      <div className=" flex justify-around items-center  ">
        <img className="navLogo" src={Logo} alt="Enyeye Logo" />
        <div className="  w-2/3 flex md:flex-row-reverse items-between md:justify-around justify-end	 ">
          {screenWidth <= 500 ? (
            <span className="border-2 p-1 rounded-full">{searchIcon}</span>
          ) : (
            SearchInputForLargeScreen
          )}
          <div className="flex ms-4">
            <ul
              className={
                screenWidth <= 750 && clicked
                  ? activeMobileNav
                  : navigationNavStyle
              }
            >
              {navMenu.map((menuItem, index) => (
                <li
                  className=" cursor-auto text-lg md:mx-6 transition duration-300 ease-in-out "
                  key={index}
                >
                  {menuItem.link}
                </li>
              ))}
            </ul>

            <span id="svg" className="md:hidden " onClick={isClicked}>
              {clicked ? burger_Icon : X_mark}{" "}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};
