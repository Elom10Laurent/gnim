// import React from "react";
import Logo from "../assets/LOGO-TOGO-GNIM.png";
import LogoGouv from "../assets/Blason-rt-768x763.png";

export const Footer = () => {
  const screenWidth = window.innerWidth;
  return (
    <div className=" bg-lime-700 rounded-sm  p-5">
      <div
        className={
          screenWidth <= 600
            ? ""
            : "md:flex flex-row-reverse items-center justify-between "
        }
      >
        <div
          className=" w-1/2 container flex justify-around
        "
        >
          <div>Lien1 </div>
          <div>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
            <p> Link bla bla 2</p>
          </div>
          <div>Lien3</div>
        </div>

        <div className=" w-1/2 flex flex-col items-center">
          <div className=" md:flex justify-center items-center">
            <img className=" h-40 w-40 " src={LogoGouv} alt="logoGouv" />
            <img className=" " src={Logo} alt="LogoTogoGnim" />
          </div>
          <div
            className={
              screenWidth <= 700
                ? "h-1 w-10/12 rounded-sm flex  items-center justify-center bg-white"
                : ""
            }
          ></div>
          <div className="">
            <p>
              MINISTÈRE DU COMMERCE, DE LARTISANAT ET DE LA CONSOMMATION
              <br />
              Un projet de la Direction de La Promotion du Commerce Électronique
            </p>

            <div className="flex items-center justify-center">
              <span className="bg-red-300 w-48 flex justify-between">
                {" "}
                <img src="" alt="" />
                kojijo
                <img src="" alt="" />
                jnkjn
                <img src="" alt="" />
                jnkjnkn
              </span>
            </div>
          </div>{" "}
        </div>
      </div>
    </div>
  );
};
