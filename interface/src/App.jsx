import { HeroBanner } from "./components/HeroBanner";
import { NavBar } from "./components/NavBar";
import { Footer } from "./components/Footer";

function App() {
  return (
    <div>
      <NavBar />
      <HeroBanner />
      <Footer />
    </div>
  );
}

export default App;
